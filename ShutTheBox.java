//Note: Wherever you see the "*" comment, it means it's for the bonus

import java.util.Scanner; //*

public class ShutTheBox
{
	public static void main(String[] args)
	{
		System.out.println("Welcome to a straightforward edition of Shut The Box :)");
		System.out.println("");
		
		boolean gameOver = false;
		boolean restartGame = true; //*
		Scanner read = new Scanner(System.in); //*
		
		//*
		int p1Wins = 0;
		int p2Wins = 0;
		
		/* This first, outer loop is only for the bonus part. If it's not wanted, only 
		   keep the while (gameOver) snippet. */
		while (restartGame)
		{
			/* For the bonus, the Board object needs to be in a loop where it is
			   re-initialized (ie, reset to scratch) upon choosing to play again */
			Board gameBoard = new Board();
			
			while (!(gameOver))
			{
				System.out.println("Player 1's turn");
				System.out.println(gameBoard);
			
				if (gameBoard.playATurn())
				{
					System.out.println("Player 2 wins!");
					System.out.println("");
					p2Wins++;
					gameOver = true;
				}
			
				else
				{
					System.out.println("Player 2's turn");
					System.out.println(gameBoard);
				
					if (gameBoard.playATurn())
					{
						System.out.println("Player 1 wins!");
						System.out.println("");
						p1Wins++;
						gameOver = true;
					}
				}
				
			}
			
			String replay ="";
			
			System.out.println("Do you want to play again? (y/n)");
			replay = read.nextLine();
			System.out.println("");
			
			// A bit of input validation
			while (!(replay.equals("y") || replay.equals("Y") || replay.equals("n") || replay.equals("N")))
			{
				System.out.println("Do you want to play again? (only enter y or n)");
				replay = read.nextLine();
				System.out.println("");
			}	
			
			if (replay.equals("y") || replay.equals("y"))
			{
				gameOver = false; // You want to get back in the game? You need to do this.
			}
			
			else
			{
				restartGame = false;
			}
			
		}
			
			//*
			System.out.println("Player 1 won: " + p1Wins + " games");
			System.out.println("Player 2 won: " + p2Wins + " games");
			
			//This next bit is just for fun.
			
			System.out.println("");
			
			if (p1Wins > p2Wins)
			{
				System.out.println("Player 1 bagged this gauntlet!");
			}
			
			else if (p2Wins > p1Wins)
			{
				System.out.println("Player 2 bagged this gauntlet!");
			}
			
			else
			{
				System.out.println("Dead even! Rematch, eventually...?");
			}
		
	}

}