public class Board
{
	
	private final int FATAL_PAIR = 3;
	private Die die1;
	private Die die2;
	private int[][] diceRolls;
	
	public Board()
	{
		this.die1 = new Die();
		this.die2 = new Die();
		this.diceRolls = new int[6][6];
	}
	
	public String toString()
	{
		String tileStatus = "\n" + "Rolled Pairs:" + "\n";
		
		// Because the outer array length is equal to all the inner arrays, i and j would be the same in a nested loop.
		
		for (int i = 0; i < this.diceRolls.length; i++)
		{
			for (int j = 0; j < this.diceRolls.length; j++)
			{
				tileStatus += (i + 1) + "," + (j + 1) + ": " + diceRolls[i][j];
				
				if (j < diceRolls.length - 1)
				{
					tileStatus += "  |  ";
				}
			}
			
			tileStatus += "\n";
			
			if (i < diceRolls.length - 1)
			{
				tileStatus += "_____________________________________________________________" + "\n";
			}
		}
		
		return tileStatus;
	}
	
	public boolean playATurn()
	{
		this.die1.roll();
		this.die2.roll();
		
		this.diceRolls[(this.die1.getPips()) - 1][(this.die2.getPips()) - 1]++;
		
		System.out.println("Your dice rolled " + this.die1 + " and " + this.die2 + ".");
		System.out.println("");
		
		
		if (this.diceRolls[(this.die1.getPips()) - 1][(this.die2.getPips()) - 1] == FATAL_PAIR)
		{
			System.out.println(toString());
			return true;
		}
		
		else
		{
			return false;
		}
		
	}
	
}